import locale
import datetime
import sys

try:
    import dateutil.tz  # pylint: disable=import-error

    _DATEUTIL_TZ = True
except ImportError:
    _DATEUTIL_TZ = False


async def load_info(hub):
    '''
    Provides
        defaultlanguage
        defaultencoding
    '''
    hub.grains.GRAINS['locale_info'] = {}

    try:
        (
            hub.grains.GRAINS['locale_info']['defaultlanguage'],
            hub.grains.GRAINS['locale_info']['defaultencoding']
        ) = locale.getdefaultlocale()
    except Exception:  # pylint: disable=broad-except
        # locale.getdefaultlocale can ValueError!! Catch anything else it
        # might do, per #2205
        hub.grains.GRAINS['locale_info']['defaultlanguage'] = 'unknown'
        hub.grains.GRAINS['locale_info']['defaultencoding'] = 'unknown'
    hub.grains.GRAINS['locale_info']['detectedencoding'] = sys.getdefaultencoding() or 'ascii'

    hub.grains.GRAINS['locale_info']['timezone'] = 'unknown'
    if _DATEUTIL_TZ:
        hub.grains.GRAINS['locale_info']['timezone'] = datetime.datetime.now(dateutil.tz.tzlocal()).tzname()
