import os


async def load_cwd(hub):
    '''
    Current working directory
    '''
    hub.grains.GRAINS['cwd'] = os.getcwd()
