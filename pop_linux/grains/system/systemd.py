import logging
import os
import shutil

log = logging.getLogger(__name__)


async def load_systemd(hub):
    if shutil.which('systemctl') and shutil.which('localectl'):
        log.debug('Adding systemd grains')
        hub.grains.GRAINS['systemd'] = {}
        systemd_info = (await hub.exec.cmd.run('systemctl --version'))['stdout'].splitlines()
        hub.grains.GRAINS['systemd']['version'] = systemd_info[0].split()[1]
        hub.grains.GRAINS['systemd']['features'] = systemd_info[1]


async def load_init(hub):
    # Add init grain
    hub.grains.GRAINS['init'] = 'unknown'
    log.debug('Adding init grain')
    try:
        os.stat('/run/systemd/system')
        hub.grains.GRAINS['init'] = 'systemd'
    except (OSError, IOError):
        try:
            with open('/proc/1/cmdline') as fhr:
                init_cmdline = fhr.read().replace('\x00', ' ').split()
        except (IOError, OSError):
            pass
        else:
            try:
                init_bin = shutil.which(init_cmdline[0])
            except IndexError:
                # Emtpy init_cmdline
                init_bin = None
                log.warning('Unable to fetch data from /proc/1/cmdline')
            if init_bin is not None and init_bin.endswith('bin/init'):
                supported_inits = (b'upstart', b'sysvinit', b'systemd')
                edge_len = max(len(x) for x in supported_inits) - 1
                buf_size = hub.opts.get('file_buffer_size', 262144)
                try:
                    with open(init_bin, 'rb') as fp_:
                        edge = b''
                        buf = fp_.read(buf_size).lower()
                        while buf:
                            buf = edge + buf
                            for item in supported_inits:
                                if item in buf:
                                    item = item.decode('utf-8')
                                    hub.grains.GRAINS['init'] = item
                                    buf = b''
                                    break
                            edge = buf[-edge_len:]
                            buf = fp_.read(buf_size).lower()
                except (IOError, OSError) as exc:
                    log.error(
                        'Unable to read from init_bin (%s): %s',
                        init_bin, exc
                    )
            elif shutil.which('supervisord') in init_cmdline:
                hub.grains.GRAINS['init'] = 'supervisord'
            elif shutil.which('dumb-init') in init_cmdline:
                # https://github.com/Yelp/dumb-init
                hub.grains.GRAINS['init'] = 'dumb-init'
            elif shutil.which('tini') in init_cmdline:
                # https://github.com/krallin/tini
                hub.grains.GRAINS['init'] = 'tini'
            elif init_cmdline == ['runit']:
                hub.grains.GRAINS['init'] = 'runit'
            elif '/sbin/my_init' in init_cmdline:
                # Phusion Base docker container use runit for srv mgmt, but
                # my_init as pid1
                hub.grains.GRAINS['init'] = 'runit'
            else:
                log.debug(
                    'Could not determine init system from command line: (%s)',
                    ' '.join(init_cmdline)
                )
