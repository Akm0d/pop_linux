import logging
import shutil

log = logging.getLogger(__name__)


async def load_selinux(hub):
    if shutil.which('selinuxenabled'):
        log.debug('Adding selinux grains')
        hub.grains.GRAINS['selinux'] = {}
        hub.grains.GRAINS['selinux']['enabled'] = (await hub.exec.cmd.run('selinuxenabled'))['retcode'] == 0

        if shutil.which('getenforce'):
            hub.grains.GRAINS['selinux']['enforced'] = (await hub.exec.cmd.run('getenforce')['stdout']).strip()
