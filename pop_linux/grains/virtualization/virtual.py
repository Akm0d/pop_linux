# Load the virtual machine info
import logging
import os
import shutil
import subprocess

log = logging.getLogger(__name__)

HAS_UNAME = True
if not hasattr(os, 'uname'):
    HAS_UNAME = False


async def load_brandz(hub):
    if hub.grains.GRAINS.get('virtual'):
        return
    # Don't waste time trying other commands to detect the virtual grain
    if HAS_UNAME and hub.grains.GRAINS['kernel'] == 'Linux' and 'BrandZ virtual linux' in os.uname():
        hub.grains.GRAINS['virtual'] = 'zone'


async def load_system_profiler(hub):
    cmd = 'system_profiler'
    if hub.grains.GRAINS.get('virtual'):
        return
    if not shutil.which(cmd):
        return

    ret = await hub.exec.cmd.run(cmd)
    if not ret['stdout'] or ret['retcode']:
        return
    output = ret['stdout'].lower()

    if '0x1ab8' in output:
        hub.grains.GRAINS['virtual'] = 'Parallels'
    elif 'parallels' in output:
        hub.grains.GRAINS['virtual'] = 'Parallels'
    elif 'vmware' in output:
        hub.grains.GRAINS['virtual'] = 'VMware'
    elif '0x15ad' in output:
        hub.grains.GRAINS['virtual'] = 'VMware'
    elif 'virtualbox' in output:
        hub.grains.GRAINS['virtual'] = 'VirtualBox'


async def system_detect_virt(hub):
    cmd = 'system-detect-virt'
    if hub.grains.GRAINS.get('virtual'):
        return
    if not shutil.which(cmd):
        return

    ret = await hub.exec.cmd.run(cmd)
    if not ret['stdout'] or ret['retcode']:
        return
    output = ret['stdout'].lower().strip()

    if output in ('qemu', 'kvm', 'oracle', 'xen', 'bochs', 'chroot', 'uml', 'systemd-nspawn'):
        hub.grains.GRAINS['virtual'] = output
    elif 'vmware' in output:
        hub.grains.GRAINS['virtual'] = 'VMware'
    elif 'microsoft' in output:
        hub.grains.GRAINS['virtual'] = 'VirtualPC'
    elif 'lxc' in output:
        hub.grains.GRAINS['virtual'] = 'LXC'
    elif 'systemd-nspawn' in output:
        hub.grains.GRAINS['virtual'] = 'LXC'


async def load_virt_what(hub):
    cmd = 'virt-what'
    if hub.grains.GRAINS.get('virtual'):
        return
    if not shutil.which(cmd):
        return

    ret = await hub.exec.cmd.run(cmd)
    if not ret['stdout'] or ret['retcode']:
        return
    output = ret['stdout'].splitlines()[-1]

    if output in ('kvm', 'qemu', 'uml', 'xen', 'lxc'):
        hub.grains.GRAINS['virtual'] = output
    elif 'vmware' in output:
        hub.grains.GRAINS['virtual'] = 'VMware'
    elif 'parallels' in output:
        hub.grains.GRAINS['virtual'] = 'Parallels'
    elif 'hyperv' in output:
        hub.grains.GRAINS['virtual'] = 'HyperV'


async def load_dmidecode(hub):
    # Product Name: VirtualBox
    cmd = 'dmidecode'
    if hub.grains.GRAINS.get('virtual'):
        return
    if not shutil.which(cmd):
        return

    ret = await hub.exec.cmd.run(cmd)
    if not ret['stdout'] or ret['retcode']:
        return
    output = ret['stdout']

    if 'Vendor: QEMU' in output:
        # FIXME: Make this detect between kvm or qemu
        hub.grains.GRAINS['virtual'] = 'kvm'
    elif 'Manufacturer: QEMU' in output:
        hub.grains.GRAINS['virtual'] = 'kvm'
    elif 'Vendor: Bochs' in output:
        hub.grains.GRAINS['virtual'] = 'kvm'
    elif 'Manufacturer: Bochs' in output:
        hub.grains.GRAINS['virtual'] = 'kvm'
    elif 'BHYVE' in output:
        hub.grains.GRAINS['virtual'] = 'bhyve'
    # Product Name: (oVirt) www.ovirt.org
    # Red Hat Community virtualization Project based on kvm
    elif 'Manufacturer: oVirt' in output:
        hub.grains.GRAINS['virtual'] = 'kvm'
        hub.grains.GRAINS['virtual_subtype'] = 'ovirt'
    # Red Hat Enterprise Virtualization
    elif 'Product Name: RHEV Hypervisor' in output:
        hub.grains.GRAINS['virtual'] = 'kvm'
        hub.grains.GRAINS['virtual_subtype'] = 'rhev'
    elif 'VirtualBox' in output:
        hub.grains.GRAINS['virtual'] = 'VirtualBox'
    # Product Name: VMware Virtual Platform
    elif 'VMware' in output:
        hub.grains.GRAINS['virtual'] = 'VMware'
    # Manufacturer: Microsoft Corporation
    # Product Name: Virtual Machine
    elif ': Microsoft' in output and 'Virtual Machine' in output:
        hub.grains.GRAINS['virtual'] = 'VirtualPC'
    # Manufacturer: Parallels Software International Inc.
    elif 'Parallels Software' in output:
        hub.grains.GRAINS['virtual'] = 'Parallels'
    elif 'Manufacturer: Google' in output:
        hub.grains.GRAINS['virtual'] = 'kvm'
    # Proxmox KVM
    elif 'Vendor: SeaBIOS' in output:
        hub.grains.GRAINS['virtual'] = 'kvm'


async def load_lspci(hub):
    cmd = 'lspci'
    if hub.grains.GRAINS.get('virtual'):
        return
    if not shutil.which(cmd):
        return

    ret = await hub.exec.cmd.run(cmd)
    if not ret['stdout'] or ret['retcode']:
        return
    output = ret['stdout']

    model = output.lower()
    if 'vmware' in model:
        hub.grains.GRAINS['virtual'] = 'VMware'
    # 00:04.0 System peripheral: InnoTek Systemberatung GmbH
    #         VirtualBox Guest Service
    elif 'virtualbox' in model:
        hub.grains.GRAINS['virtual'] = 'VirtualBox'
    elif 'qemu' in model:
        hub.grains.GRAINS['virtual'] = 'kvm'
    elif 'virtio' in model:
        hub.grains.GRAINS['virtual'] = 'kvm'


async def load_prtdiag(hub):
    cmd = 'prtdiag'
    if hub.grains.GRAINS.get('virtual'):
        return
    if not shutil.which(cmd):
        return

    ret = await hub.exec.cmd.run(cmd)
    if not ret['stdout'] or ret['retcode']:
        return
    output = ret['stdout']

    model = output.lower().split("\n")[0]
    if 'vmware' in model:
        hub.grains.GRAINS['virtual'] = 'VMware'
    elif 'virtualbox' in model:
        hub.grains.GRAINS['virtual'] = 'VirtualBox'
    elif 'qemu' in model:
        hub.grains.GRAINS['virtual'] = 'kvm'
    elif 'joyent smartdc hvm' in model:
        hub.grains.GRAINS['virtual'] = 'kvm'


async def load_virtinfo(hub):
    cmd = 'prtdiag'
    if hub.grains.GRAINS.get('virtual'):
        return
    if not shutil.which(cmd):
        return

    hub.grains.GRAINS['virtual'] = 'LDOM'


async def check_virtual(hub):
    # If we have a virtual_subtype, we're virtual, but maybe we couldn't
    # figure out what specific virtual type we were?
    if hub.grains.GRAINS.get('virtual_subtype') and hub.grains.GRAINS.get('virtual') == 'physical':
        hub.grains.GRAINS['virtual'] = 'virtual'
