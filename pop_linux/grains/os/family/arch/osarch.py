import platform
import shutil
import subprocess


async def _get_rpm_osarch():
    '''
    Get the os architecture using rpm --eval
    '''
    if shutil.which('rpm'):
        ret = subprocess.Popen(
            'rpm --eval "%{_host_cpu}"',
            shell=True,
            close_fds=True,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE).communicate()[0]
    else:
        ret = ''.join([x for x in platform.uname()[-2:] if x][-1:])

    return str(ret).strip() or 'unknown'


async def load_arch(hub):
    # Build the osarch grain. This grain will be used for platform-specific
    # considerations such as package management. Fall back to the CPU
    # architecture.
    if hub.grains.GRAINS.get('os_family') == 'Debian':
        osarch = (await hub.exec.cmd.run('dpkg --print-architecture'))['stdout'].strip()
    elif hub.grains.GRAINS.get('os_family') in ['RedHat', 'Suse']:
        osarch = await _get_rpm_osarch()
    elif hub.grains.GRAINS.get('os_family') in ('NILinuxRT', 'Poky'):
        archinfo = {}
        for line in (await hub.exec.cmd.run(['opkg', 'print-architecture'])['stdout']).splitlines():
            if line.startswith('arch'):
                _, arch, priority = line.split()
                archinfo[arch.strip()] = int(priority.strip())

        # Return osarch in priority order (higher to lower)
        osarch = sorted(archinfo, key=archinfo.get, reverse=True)
    else:
        osarch = hub.grains.GRAINS['cpuarch']
    hub.grains.GRAINS['osarch'] = osarch

