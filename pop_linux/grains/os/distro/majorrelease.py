import logging

log = logging.getLogger(__name__)


async def load_release(hub):
    osrelease_info = hub.grains.GRAINS['osrelease'].split('.')
    for idx, value in enumerate(osrelease_info):
        if not value.isdigit():
            continue
        osrelease_info[idx] = int(value)
    hub.grains.GRAINS['osrelease_info'] = tuple(osrelease_info)
    try:
        hub.grains.GRAINS['osmajorrelease'] = int(hub.grains.GRAINS['osrelease_info'][0])
    except (IndexError, TypeError, ValueError):
        log.debug(
            'Unable to derive osmajorrelease from osrelease_info \'%s\'. '
            'The osmajorrelease grain will not be set.',
            hub.grains.GRAINS['osrelease_info']
        )
    os_name = hub.grains.GRAINS['os' if hub.grains.GRAINS.get('os') in (
        'Debian', 'FreeBSD', 'OpenBSD', 'NetBSD', 'Mac', 'Raspbian') else 'osfullname']
    finger = hub.grains.GRAINS['osrelease'] if os_name in ('Ubuntu',) else hub.grains.GRAINS['osrelease_info'][0]
    hub.grains.GRAINS['osfinger'] = f'{os_name}-{finger}'
