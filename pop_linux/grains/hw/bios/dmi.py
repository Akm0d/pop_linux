'''
Get system specific hardware data from dmidecode

Provides
    biosversion
    productname
    manufacturer
    serialnumber
    biosreleasedate
    uuid

.. versionadded:: 0.9.5
'''
import errno
import logging
import os
import re
import shutil
import uuid

log = logging.getLogger(__name__)


async def _clean_value(key, val):
    '''
    Clean out well-known bogus values.
    If it isn't clean (for example has value 'None'), return None.
    Otherwise, return the original value.

    NOTE: This logic also exists in the smbios module. This function is
          for use when not using smbios to retrieve the value.
    '''
    if (val is None or not val or
            re.match('none', val, flags=re.IGNORECASE)):
        return None
    elif 'uuid' in key:
        # Try each version (1-5) of RFC4122 to check if it's actually a UUID
        for uuidver in range(1, 5):
            try:
                uuid.UUID(val, version=uuidver)
                return val
            except ValueError:
                continue
        log.debug('HW %s value %s is an invalid UUID', key, val.replace('\n', ' '))
        return None
    elif re.search('serial|part|version', key):
        # 'To be filled by O.E.M.
        # 'Not applicable' etc.
        # 'Not specified' etc.
        # 0000000, 1234567 etc.
        # begone!
        if (re.match(r'^[0]+$', val) or
                re.match(r'[0]?1234567[8]?[9]?[0]?', val) or
                re.search(r'sernum|part[_-]?number|specified|filled|applicable', val, flags=re.IGNORECASE)):
            return None
    elif re.search('asset|manufacturer', key):
        # AssetTag0. Manufacturer04. Begone.
        if re.search(r'manufacturer|to be filled|available|asset|^no(ne|t)', val, flags=re.IGNORECASE):
            return None
    else:
        # map unspecified, undefined, unknown & whatever to None
        if (re.search(r'to be filled', val, flags=re.IGNORECASE) or
                re.search(r'un(known|specified)|no(t|ne)? (asset|provided|defined|available|present|specified)',
                          val, flags=re.IGNORECASE)):
            return None
    return val


async def load_dmi(hub):
    name = '/sys/class/dmi/id'
    if os.path.exists(name):
        # On many Linux distributions basic firmware information is available via sysfs
        # requires CONFIG_DMIID to be enabled in the Linux kernel configuration
        sysfs_firmware_info = {
            'biosversion': 'bios_version',
            'productname': 'product_name',
            'manufacturer': 'sys_vendor',
            'biosreleasedate': 'bios_date',
            'uuid': 'product_uuid',
            'serialnumber': 'product_serial'
        }
        for key, fw_file in sysfs_firmware_info.items():
            contents_file = os.path.join('/sys/class/dmi/id', fw_file)
            if os.path.exists(contents_file):
                try:
                    with open(contents_file, 'r') as ifile:
                        hub.grains.GRAINS[key] = ifile.read().strip()
                        if key == 'uuid':
                            hub.grains.GRAINS['uuid'] = hub.grains.GRAINS['uuid'].lower()
                except (IOError, OSError) as err:
                    # PermissionError is new to Python 3, but corresponds to the EACESS and
                    # EPERM error numbers. Use those instead here for PY2 compatibility.
                    if err.errno == errno.EACCES or err.errno == errno.EPERM:
                        # Skip the grain if non-root user has no access to the file.
                        pass


async def load_smbios(hub):
    if shutil.which('smbios'):
        hub.grains.GRAINS.update({
            'biosversion': hub.exec.smbios.get('bios-version'),
            'productname': hub.exec.smbios.get('system-product-name'),
            'manufacturer': hub.exec.smbios.get('system-manufacturer'),
            'biosreleasedate': hub.exec.smbios.get('bios-release-date'),
            'uuid': hub.exec.smbios.get('system-uuid')
        })
        grains = dict([(key, val) for key, val in hub.grains.GRAINS.items() if val is not None])
        uuid = hub.exec.smbios.get('system-uuid')
        if uuid is not None:
            grains['uuid'] = uuid.lower()
        for serial in ('system-serial-number', 'chassis-serial-number', 'baseboard-serial-number'):
            serial = hub.exec.smbios.get(serial)
            if serial is not None:
                grains['serialnumber'] = serial
                break


async def load_arm_linux(hub):
    if shutil.which('fw_printenv'):
        # ARM Linux devices expose UBOOT env variables via fw_printenv
        hwdata = {
            'manufacturer': 'manufacturer',
            'serialnumber': 'serial#',
            'productname': 'DeviceDesc',
        }
        for grain_name, cmd_key in hwdata.items():
            result = await hub.exec.cmd.run(f'fw_printenv {cmd_key}')
            if result['retcode'] == 0:
                uboot_keyval = result['stdout'].split('=')
                hub.grains.GRAINS[grain_name] = (await _clean_value(grain_name, uboot_keyval)[1])
