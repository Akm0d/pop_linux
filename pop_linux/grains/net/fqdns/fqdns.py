import logging
import socket

log = logging.getLogger(__name__)

# Possible value for h_errno defined in netdb.h
HOST_NOT_FOUND = 1
NO_DATA = 4


async def load_fqdns(hub):
    '''
    Return all known FQDNs for the system by enumerating all interfaces and
    then trying to reverse resolve them (excluding 'lo' interface).
    '''
    # Provides:
    # fqdns
    fqdns = set()

    addresses = hub.grains.GRAINS['ip_interfaces']

    err_message = 'An exception occurred resolving address \'%s\': %s'
    for ip in addresses:
        try:
            fqdns.add(socket.getfqdn(socket.gethostbyaddr(ip)[0]))
        except socket.herror as err:
            if err.errno in (0, HOST_NOT_FOUND, NO_DATA):
                # No FQDN for this IP address, so we don't need to know this all the time.
                log.debug("Unable to resolve address %s: %s", ip, err)
            else:
                log.error(err_message, ip, err)
        except (socket.error, socket.gaierror, socket.timeout) as err:
            log.debug(err_message, ip, err)

    hub.grains.GRAINS['fqdns'] = sorted(fqdns)
