import asyncio
import logging
import os
import subprocess
from typing import Any, Dict, List

log = logging.getLogger(__name__)

__virtualname__ = 'cmd'


async def run(hub,
              cmd: str or List[str],
              cwd: str = None,
              stdin: str = None,  # TODO? Is this necessary with python3 subprocess tools?
              stdout: int = asyncio.subprocess.PIPE,
              stderr: int = asyncio.subprocess.PIPE,
              shell: bool = False,
              env: Dict[str, Any] = None,
              umask: str = None,
              timeout: int = None,
              **kwargs):
    '''
    Execute the passed command and return the output as a string

    :param cmd: The command to run. ex: ``ls -lart /home``

    :param cwd: The directory from which to execute the command. Defaults
        to the home directory of the user specified by ``runas`` (or the user
        under which Salt is running if ``runas`` is not specified).

    :param stdin: A string of standard input can be specified for the
        command to be run using the ``stdin`` parameter. This can be useful in
        cases where sensitive information must be read from standard input.

    :param shell: If ``False``, let python handle the positional
        arguments. Set to ``True`` to use shell features, such as pipes or
        redirection.

    :param env: Environment variables to be set prior to execution.

        .. note::
            When passing environment variables on the CLI, they should be
            passed as the string representation of a dictionary.

            .. code-block:: bash

                salt myminion cmd.run 'some command' env='{"FOO": "bar"}'
    :param umask: The umask (in octal) to use when running the command.

    :param timeout: A timeout in seconds for the executed process to return.
    '''

    # salt-minion is running as. Defaults to home directory of user under which
    # the minion is running.
    if not cwd:
        cwd = os.path.expanduser('~')

        # make sure we can access the cwd
        # when run from sudo or another environment where the euid is
        # changed ~ will expand to the home of the original uid and
        # the euid might not have access to it. See issue #1844
        if not os.access(cwd, os.R_OK):
            cwd = '/'
    else:
        # Handle edge cases where numeric/other input is entered, and would be
        # yaml-ified into non-string types
        cwd = str(cwd)

    ret = {}

    if env is not None:
        for bad_env_key in (k for k, v in env.items() if v is None):
            log.error('Environment variable \'%s\' passed without a value. '
                      'Setting value to an empty string', bad_env_key)
            env[bad_env_key] = ''

    # Return stripped command string copies to improve logging.
    if isinstance(cmd, list):
        cmd = [x.strip() for x in cmd]

    new_kwargs = {'cwd': cwd,
                  'shell': shell,
                  'env': env if env else os.environ.copy(),
                  'stdout': stdout,
                  'stderr': stderr,
                  }
    if 'stdin_raw_newlines' in kwargs:
        new_kwargs['stdin_raw_newlines'] = kwargs['stdin_raw_newlines']

    if umask is not None:
        _umask = str(umask).lstrip('0')

        if _umask == '':
            msg = 'Zero umask is not allowed.'
            raise SystemError(msg)

        try:
            _umask = int(_umask, 8)
        except ValueError:
            raise SystemError("Invalid umask: '{0}'".format(umask))
    else:
        _umask = None

    if not os.path.isabs(cwd) or not os.path.isdir(cwd):
        raise SystemError(f'Specified cwd \'{cwd}\' either not absolute or does not exist')

    if shell is not True and not isinstance(cmd, list):
        cmd = cmd.split()

    # This is where the magic happens
    proc = await asyncio.create_subprocess_exec(*cmd, **new_kwargs)
    out, err = await asyncio.wait_for(proc.communicate(input=stdin), timeout=timeout)

    ret['pid'] = proc.pid
    ret['retcode'] = proc.returncode
    ret['stdout'] = out.decode()
    ret['stderr'] = err.decode()

    return ret
